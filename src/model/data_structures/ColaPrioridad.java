package model.data_structures;

public class ColaPrioridad<T extends Comparable<T>> {
	private Object[] holi;
	private int usados;
	private int maxi;

	public void crearCP(int max) {
		holi = new Object[max];
		usados = 0;
		maxi = max;
	}

	public int darNumeroElementos() {
		return usados;
	}

	public void mover1(int j) {
		for (int i = holi.length - 1; i > j; i--) {
			holi[i] = holi[i - 1];
		}
	}

	public void agregar(T elemento) throws Exception {
		boolean bool = false;
		if (usados == maxi)
			throw new Exception("Cola de prioridad llena");
		if (usados == 0) {
			holi[0] = elemento;
			usados++;
			bool = true;
		}
		for (int i = 0; i < usados && !bool; i++) {
			if (((T) holi[i]).compareTo(elemento) > 0) {
				mover1(i);
				holi[i] = elemento;
				bool = true;
				usados++;
			}
		}
		if (!bool) {
			holi[usados] = elemento;
			usados++;
		}
	}

	public T max() {
		if (usados != 0) {
			T hl = (T) holi[usados - 1];
			holi[usados - 1] = null;
			usados--;
			return hl;
		}
		return null;
	}

	public boolean esVacia() {
		return usados == 0;
	}

	public int tamanoMax() {
		return maxi;
	}
}