package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import model.VO.VOInfoPelicula;
import model.VO.VOSort;
import model.data_structures.ColaPrioridad;
import model.data_structures.ListaEncadenada;
import model.data_structures.RedBlackBST;

public class SistemaRecomendador {
	public static final String RUTA = "./data/links_json.json";
	RedBlackBST<Integer, RedBlackBST<Integer,VOInfoPelicula>> gBlackBST=new RedBlackBST<>();
	public void cargarDatos(String ruta) {
		try {
			FileReader fr = new FileReader(ruta);
			BufferedReader buff = new BufferedReader(fr);
			JsonArray ja = (JsonArray) (new JsonParser()).parse(buff);

			for (JsonElement jsonElement : ja) {

				JsonObject jo = (JsonObject) jsonElement;
				JsonElement id = jo.get("movieId");
				int movieId = id.getAsInt();

				JsonObject info = (JsonObject) jo.get("imdbData");

				String anio = info.get("Year").getAsString().substring(0, 4);
				int up = Integer.parseInt(anio);

				String rating = info.get("imdbRating").getAsString();
				double j = 0;
				if (!rating.equals("N/A"))
					j = Double.parseDouble(rating);

				VOInfoPelicula h = new VOInfoPelicula(info.get("Title").getAsString(), up,
						info.get("Director").getAsString(), info.get("Actors").getAsString(), j,movieId);
				
				if(gBlackBST.contains(h.getAnio())){
					(gBlackBST.get(h.getAnio())).put(h.getId(), h);
				}
				else{
					RedBlackBST<Integer, VOInfoPelicula>gg=new RedBlackBST<>();
					gg.put(h.getId(), h);
					gBlackBST.put(h.getAnio(), gg);
				}

				buff.close();
				fr.close();
			}
		} catch (Exception e) {
			System.err.println(e);
		}
	}
	
	public ListaEncadenada<VOInfoPelicula> peliculasEntre(int anioInicial, int anioFinal){
		ListaEncadenada<VOInfoPelicula> temp=new ListaEncadenada<>();
		Iterable<Integer> j=gBlackBST.keys(anioInicial, anioFinal);
		for (int llaveAnio : j) {
			RedBlackBST<Integer, VOInfoPelicula> h=gBlackBST.get(llaveAnio);
			Iterable<Integer> k=null;
			try {
				k=h.keys();
			} catch (Exception e) {
				e.printStackTrace();
			}
			for (Integer llaveIMDB : k) {
				temp.agregarElementoFinal(h.get(llaveIMDB));
			}
		}
		return temp;
	}
	
	public ListaEncadenada<VOSort> ordenadaPorCantidad(){
		ListaEncadenada<VOSort> g=new ListaEncadenada<>();
		ColaPrioridad<VOSort> temp=new ColaPrioridad<>();
		temp.crearCP(200);
		try {
			Iterable<Integer> j=gBlackBST.keys();
			for (Integer integer : j) {
				VOSort h=new VOSort();
				h.setAnio(integer);
				h.setHoli(gBlackBST.get(integer));
				temp.agregar(h);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		int j=temp.darNumeroElementos();
		for (int i = 0; i < j; i++) {
			g.agregarElementoFinal(temp.max());
		}
		return g;
	}
	public ListaEncadenada<VOInfoPelicula> inOrden(int anio){
		ListaEncadenada<VOInfoPelicula> temp=new ListaEncadenada<>();
		try {
			Iterable<Integer> k =gBlackBST.get(anio).keys();
			for (Integer integer : k) {
				temp.agregarElementoFinal(gBlackBST.get(anio).get(integer));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return temp;
	}
	
//	public static void main(String[] args) {
//		SistemaRecomendador h= new SistemaRecomendador();
//		h.cargarDatos("./data/links_json.json");
//		System.out.println(h.peliculasEntre(2000, 2002));
//		System.out.println(h.ordenadaPorCantidad());
//		System.out.println(h.inOrden(2016));
//	}
}
