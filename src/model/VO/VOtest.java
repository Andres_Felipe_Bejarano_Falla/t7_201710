package model.VO;

public class VOtest<K extends Comparable<K>, V> {
	private K llave;
	private V valor;

	public VOtest(K llave, V valor) {
		this.llave = llave;
		this.valor = valor;
	}

	public K getLlave() {
		return llave;
	}

	public void setLlave(K llave) {
		this.llave = llave;
	}

	public V getValor() {
		return valor;
	}

	public void setValor(V valor) {
		this.valor = valor;
	}

	public String toString() {
		return "VOtest [llave=" + llave + ", valor=" + valor + "]";
	}

}
