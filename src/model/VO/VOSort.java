package model.VO;

import model.data_structures.RedBlackBST;

public class VOSort implements Comparable<VOSort>{
	
	int anio;
	RedBlackBST<Integer, VOInfoPelicula> holi;

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public RedBlackBST<Integer, VOInfoPelicula> getHoli() {
		return holi;
	}

	public void setHoli(RedBlackBST<Integer, VOInfoPelicula> holi) {
		this.holi = holi;
	}

	public int compareTo(VOSort o) {
		if(holi.size()<o.holi.size())return -1;
		if(holi.size()>o.holi.size())return 1;
		else return 0;
	}

	public String toString() {
		return "VOSort [anio=" + anio + ", holi=" + holi.size() + "]";
	}

}
