package test;

import java.util.ArrayList;

import junit.framework.TestCase;
import model.VO.VOtest;
import model.data_structures.RedBlackBST;
import model.logic.GeneradorDatos;

public class test extends TestCase{
	private RedBlackBST<Integer, String> lista;
	private ArrayList<VOtest<Integer, String>> listaTemp;
	private GeneradorDatos gd = new GeneradorDatos();

	private void setupEscenario1() {
		lista = new RedBlackBST<Integer, String>();
	}

	private void setupEscenario2() {
		lista = new RedBlackBST<Integer, String>();
		listaTemp = new ArrayList<>();
		try {
			lista.put(12, "a");
			listaTemp.add(new VOtest<Integer, String>(12, "a"));
			lista.put(2, "b");
			listaTemp.add(new VOtest<Integer, String>(2, "b"));
			lista.put(120, "c");
			listaTemp.add(new VOtest<Integer, String>(120, "c"));
			lista.put(15, "d");
			listaTemp.add(new VOtest<Integer, String>(15, "d"));
			lista.put(24, "e");
			listaTemp.add(new VOtest<Integer, String>(24, "e"));
		} catch (Exception e) {
			fail();
		}
	}

	private void setupEscenario3() {
		lista = new RedBlackBST<Integer, String>();
		listaTemp = new ArrayList<>();
		String[] h = gd.generarCadenas(100);
		for (int i = 0; i < 100; i++) {
			try {
				lista.put(i, h[i]);
				listaTemp.add(new VOtest<Integer, String>(i, h[i]));
			} catch (Exception e) {
				fail();
			}
		}
	}

	public void testGet1() {
		setupEscenario1();
		try {
			assertNull(lista.get(10));
		} catch (Exception e) {
			fail();
		}
	}

	public void testGet2() {
		setupEscenario1();
		try {
			assertNull(lista.get(null));
			fail();
		} catch (Exception e) {
		}
	}

	public void testGet3(){
		setupEscenario2();
		for (int i = 0; i < listaTemp.size(); i++) {
			String p=listaTemp.get(i).getValor();
			Integer u=listaTemp.get(i).getLlave();
			try {
				assertEquals(p, lista.get(u));
			} catch (Exception e) {
				fail();
			}
		}
	}

	public void testGet4(){
		setupEscenario3();
		for (int i = 0; i < listaTemp.size(); i++) {
			String p=listaTemp.get(i).getValor();
			Integer u=listaTemp.get(i).getLlave();
			try {
				assertEquals(p, lista.get(u));
			} catch (Exception e) {
				fail();
			}
		}
	}

	public void testAgregar(){
		setupEscenario1();
		try {
			lista.put(2, "lol");
			assertEquals("lol",lista.get(2));
		} catch (Exception e) {
			fail();
		}
	}

	public void testAgregar2(){
		setupEscenario2();
		try {
			assertEquals("b",lista.get(2));
			lista.put(2, "lol");
			assertEquals("lol",lista.get(2));
		} catch (Exception e) {
			fail();
		}
	}

	public void testAgregar3(){
		setupEscenario3();
		try {
			lista.put(2, "lol");
			assertEquals("lol",lista.get(2));
		} catch (Exception e) {
			fail();
		}
	}

	public void testSizeIsEmpty(){

		//1
		setupEscenario1();
		assertEquals(0, lista.size());
		assertTrue(lista.isEmpty());

		//2
		setupEscenario2();
		assertEquals(5, lista.size());
		assertFalse(lista.isEmpty());

		//3
		setupEscenario3();
		assertEquals(100, lista.size());
		assertFalse(lista.isEmpty());
	}
	public void testCheck(){
		//2
		setupEscenario2();
		try {
			assertTrue(lista.check());
		} catch (Exception e) {
			e.printStackTrace();
			fail("1");
		}

		//3
		setupEscenario3();
		try {
			assertTrue(lista.check());
		} catch (Exception e) {
			fail("2");
		}
	}
	public void testMaxMin(){
		setupEscenario2();
		try {
			assertEquals(2, (int)lista.min());
			assertEquals(120, (int)lista.max());
		} catch (Exception e) {
			fail();
		}
	}
	public void testMaxMinDel(){
		setupEscenario2();
		try {
			lista.deleteMin();
		} catch (Exception e) {
			fail();
		}
		try {
			lista.deleteMax();
		} catch (Exception e) {
			fail();
		}
		assertEquals(3,lista.size());
	}
}
